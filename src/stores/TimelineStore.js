import TimelineActions from 'actions/TimelineActions';
import { Alt, Storage } from 'utils';

@Storage
class TimelineStore {

	constructor() {
		this.markers = this.get('markers') || [];
		this.time = this.get('time') || 0;

		this.bindListeners({
			onInit: TimelineActions.init,
			onSetMarkers: TimelineActions.addMarker,
			onClearMarkers: TimelineActions.clearMarkers
		});
	}

	onInit = (time) => {
		this.time = parseInt(time);
		this.set('time', this.time);
	}

	onSetMarkers = (marker) => {
		this.markers.push(marker);
		this.set('markers', this.markers);
	}

	onClearMarkers = () => {
		this.markers = [];
		this.set('markers', []);
	}
}

export default Alt.createStore(TimelineStore, 'TimelineStore');