import React from 'react';
import { Route } from 'react-router';
import { GenerateRoute } from './utils';

export default (
	<Route component={ require('./pages/Homepage') }>
		{ GenerateRoute({
			paths: ['/'],
			component: require('./pages/Homepage')
		}) }
	</Route>
);