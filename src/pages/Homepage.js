import React, {Component, PropTypes} from 'react';
import {Controlls, Timeline} from 'components';

import TimelineStore from 'stores/TimelineStore'

class Homepage extends Component {

	constructor() {
		super();
		this.state = {
			time: TimelineStore.getState().time,
			markers: TimelineStore.getState().markers
		}
	}

	componentDidMount() {
		TimelineStore.listen(this.handleStore);
	}

	componentWillUnmount() {
		TimelineStore.unlisten(this.handleStore);
	}

	handleStore = (store) => {
		this.setState({
			time: store.time,
			markers: store.markers
		})
	};

	render() {
		const {time, markers} = this.state;
		return (
			<div>
				<div className="clearfix">
					<Controlls/>
					{ this.state.time > 0 ? <Timeline/> : null }
				</div>
			</div>
		);
	}
}

export default Homepage;