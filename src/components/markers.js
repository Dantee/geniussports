import React, {Component, PropTypes} from 'react';

import TimelineStore from 'stores/TimelineStore';
import {Marker} from 'components';

class Markers extends Component {

	constructor() {
		super();
		this.state = {
			markers: TimelineStore.getState().markers,
			time: TimelineStore.getState().time
		};
	}

	componentDidMount() {
		TimelineStore.listen(this.handleStore);
	}

	componentWillUnmount() {
		TimelineStore.unlisten(this.handleStore);
	}

	handleStore = (store) => {
		this.setState({
			time: store.time,
			markers: store.markers
		});
	};

	getMarkers = (markers, team) => {
		return markers.filter((marker) => {
			return marker.team == team;
		});
	};

	render() {
		const {markers} = this.state;
		return (
			<div className="markers">
				<Marker markers={this.getMarkers(markers, 'HOME')} team="home" time={this.state.time}/>
				<Marker markers={this.getMarkers(markers, 'AWAY')} team="away" time={this.state.time}/>
			</div>
		);
	}
}

export default Markers;