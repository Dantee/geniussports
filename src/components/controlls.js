import React, { Component, PropTypes } from 'react';

import TimelineActions from 'actions/TimelineActions'
import TimelineStore from 'stores/TimelineStore';

class Controlls extends Component {

	constructor() {
		super();
		this.state = {
			markers: TimelineStore.getState().markers,
			time: TimelineStore.getState().time
		};
	}

	componentDidMount() {
		TimelineStore.listen(this.handleStore);
		window.init = (lengthOfPeriodInSeconds) => {
			if (lengthOfPeriodInSeconds > 0) {
				this.setState({
					initialTime: lengthOfPeriodInSeconds
				});
				this.initTime();
			} else {
				alert('Time value must be higher than 0.');
			}
		};
		window.addAction = (timeInSeconds, team) => {
			this.pushMarker(team, timeInSeconds);
		}
	}

	componentWillUnmount() {
		TimelineStore.unlisten(this.handleStore);
	}

	handleStore = (store) => {
		this.setState({
			time: store.time,
			markers: store.markers
		})
	};

	initTime = () => {
		TimelineActions.init(this.state.initialTime);
		TimelineActions.clearMarkers();
	};

	setMarker = (e) => {
		this.setState({
			markerPos: e.target.value
		});
	};

	setTime = (e) => {
		this.setState({
			initialTime: e.target.value
		})
	};

	pushMarker = (team, t) => {
		let pos = t ? t : parseInt(this.state.markerPos);
		let time = parseInt(this.state.time);
		if (pos >= 0 && pos <= time) {
			TimelineActions.addMarker({
				team: team,
				time: t ? t : pos
			});
		} else {
			alert('Marker goes out of bounds!');
		}
	};

	render() {
		return (
			<div className="controlls">
				<div className="init">
					<input type="number" min="0" onChange={ this.setTime }/>
					<button onClick={ this.initTime }>Set length of period</button>
				</div>
				<div className="set">
					<input type="number" min="0" onChange={ this.setMarker }/>
					<button onClick={ () => {this.pushMarker('HOME')} }>Set HOME</button>
					<button onClick={ () => {this.pushMarker('AWAY')} }>Set AWAY</button>
				</div>
			</div>
		);
	}
}

export default Controlls;