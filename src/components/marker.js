import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';

class Marker extends Component {
	static propTypes = {
		markers: PropTypes.array.isRequired,
		team: PropTypes.string.isRequired,
		time: PropTypes.number.isRequired
	};

	constructor() {
		super();
		this.homeClass = 'marker-home';
		this.awayClass = 'marker-away';
		this.state = {
			away: [],
			home: [],
			markers: []
		};
		this.overlaps = {};
		this.forced = true;
	}

	componentDidMount() {
		this.resetMarkers();
		window.addEventListener('resize', this.resetMarkers.bind(this));
	}

	componentWillReceiveProps() {
		this.resetMarkers();
	}

	getPositionArray = (teamClass) => {
		let homeMarkers = document.getElementsByClassName(teamClass);
		let positions = [], team;
		for (let i = 0; i < homeMarkers.length; i++) {
			let marker = homeMarkers[i];
			let markerTime = marker.dataset.time;
			let offLeft = marker.offsetLeft;
			let offRight = marker.offsetWidth + offLeft;
			positions.push({
				offLeft: offLeft,
				offRight: offRight,
				time: markerTime
			});
			team = marker.dataset.team.toLowerCase();
		}
		let obj = {};
		if (positions.length) {
			obj[team] = positions;
			this.setState(obj, () => {
				this.setMarkers();
			});
		} else {
			this.setMarkers();
		}
	};

	countMarkers = (arr) => {
		for (let i = 0; i < this.props.markers.length; i++) {
			let marker = this.props.markers[i];
			let index = -1;
			marker.count = 1;

			for (let i = 0; i < arr.length; i++) {
				if (arr[i].time == marker.time) {
					index = i;
					break;
				}
			}
			if (index == -1) {
				arr.push(marker);
			} else {
				arr[index].count += 1;
			}
		}
	};

	resetMarkers = () => {
		var arr = [];
		this.countMarkers(arr);
		this.setState({
			markers: arr
		}, () => {
			this.getPositionArray(this.homeClass);
			this.getPositionArray(this.awayClass);
		});
	};


	setMarkers = () => {
		let markerArray = [];
		this.countMarkers(markerArray);
		if (this.state[this.props.team]) {
			let positions = this.state[this.props.team];
			let overlappingTimes = [];
			let added = [];
			let arr = [];

			let checkForOverlap = (pos1) => {
				let posLeft = pos1.offLeft;
				let posRight = pos1.offRight;
				for (let i = 0; i < positions.length; i++) {
					let pos2 = positions[i];
					if ((posLeft >= pos2.offLeft && posLeft <= pos2.offRight) ||
						(posRight >= pos2.offLeft && posRight <= pos2.offRight) ||
						(posLeft <= pos2.offLeft && posLeft <= pos2.offRight && posRight >= pos2.offLeft && posRight >= pos2.offRight)
					) {
						if (pos1.time !== pos2.time) {
							if (arr.length == 0) {
								arr.push(pos1.time);
							}
							if (arr.indexOf(pos2.time) == -1) {
								added.push(pos2.time);
								arr.push(pos2.time);
								checkForOverlap(pos2);
							}
						}
					}
				}
			};

			for (let i = 0; i < positions.length; i++) {
				let pos1 = positions[i];
				if (added.indexOf(pos1.time) == -1) {
					checkForOverlap(pos1);
				}
				if (arr.length > 1) {
					overlappingTimes.push(arr);
				}
			}
			this.overlaps[this.props.team] = overlappingTimes;
			if (this.overlaps[this.props.team]) {
				let overlaps = this.overlaps[this.props.team];
				for (let i = 0; i < overlaps.length; i++) {
					let overlap = overlaps[i];
					let newMarker = {
						time: []
					};
					for (let i = 0; i < markerArray.length; i++) {
						let markerTime = markerArray[i].time;
						if (overlap.indexOf(markerTime.toString()) !== -1) {
							newMarker.time.push(markerTime);
							newMarker.count = newMarker.count ? newMarker.count + markerArray[i].count : markerArray[i].count;
							newMarker.team = markerArray[i].team;
							markerArray.splice(i, 1);
							i = i - 1;
						}
					}
					if (newMarker.team && newMarker.time) {
						newMarker.time = newMarker.time.reduce(function(a, b){ return a + b }) / newMarker.time.length;
						markerArray.push(newMarker);
					}
				}
			}
		}
		this.setState({
			markers: markerArray
		});
	};

	render() {
		const {team, time} = this.props;
		const {markers} = this.state;
		return (
			<div className={'markers-' + team}> {
				markers.map((marker, i) => {
					let pos = marker.time / time * 100;
					let style = {left: pos + '%'};
					if (marker.count > 1) {
						return <div className={'marker counter marker-' + marker.team.toLowerCase()}
									data-time={marker.time}
									data-team={marker.team}
									style={style}
									key={i}>{marker.count}</div>
					} else {
						return <div className={'marker marker-' + marker.team.toLowerCase()}
									data-time={marker.time}
									data-team={marker.team}
									style={style}
									key={i}></div>
					}
				})}
			</div>
		)
	}
}

export default Marker;