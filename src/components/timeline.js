import React, {Component, PropTypes} from 'react';
import {Markers} from 'components';

import TimelineStore from 'stores/TimelineStore';

class Timeline extends Component {
	constructor() {
		super();
		this.state = {
			steps: Array.from(new Array(10), (val, index)=> index),
			markers: []
		};
	}

	componentDidMount() {
		TimelineStore.listen(this.handleStore);
	}

	componentWillUnmount() {
		TimelineStore.unlisten(this.handleStore);
	}

	handleStore = (store) => {
		this.setState({
			time: store.time,
			markers: store.markers
		})
	};

	render() {
		const {steps} = this.state;
		const style = {width: 100 / steps.length + '%'};
		return (
			<div className="timeline">
				<div className="wrap clearfix">
					<Markers/>
					<div className="period-wrap">
						{
							steps.map((step, i) => {
								return (
									<span className="period" key={ i } style={ style }>
								</span>
								)
							})
						}
					</div>
				</div>
			</div>
		);
	}
}

export default Timeline;
