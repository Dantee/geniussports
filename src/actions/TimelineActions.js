import { Alt } from 'utils';

class TimelineActions {
	
	init(time) {
		return time;
	}

	addMarker(marker) {
		return marker;
	}

	clearMarkers() {
		return null;
	}

}

export default Alt.createActions(TimelineActions);
